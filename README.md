'''
	Python Version: 3.7

	This script is run as a scheduled task on Windows 10 where this script reads the route part of the module file of an Angular.js application and replaces the ip address configured
	in it. Before, due to circumstances the server hosting a Laravel/Angular.js app is not yet configured a static IP and therefore inaccessible by the other PCs
	in the network because the IP always changing (due to being connected to other ports etc). This script checks the current IP assigned to that server PC and overwrites
	the IP address route configured in the module.js file of the Angular.js application. It also updates the defined URL of Google Chrome bookmark.

	Example:
	It's defined in the module.js file that the environment to be used is localhost in the form eg: https://localhost/<laravel app dir>/public/api/getDeliveryReceipts.

	When deployed to the server, the route is changed to https://<server LAN ipaddress eg. 192.168.0.10>/<laravel app dir>/public/api/getDeliveryReceipts.

	This python script reads module.js and regex-matches a string with an IP format (192.168.0.10) and replaces it with the current IP address (regardless if it changed or not)

	This is scheduled in Windows 10 to run on boot.
'''
